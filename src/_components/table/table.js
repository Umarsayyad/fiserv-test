import React, { useState, useEffect } from 'react';

import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { Button, Modal, ModalDialog } from 'react-bootstrap';


export const Table = () => {

  const [nasa, setNasa] = useState([]);
  const [modalInfo, setModalInfo] = useState([]);
  const [showModal, setShowModal] = useState(false);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () =>  setShow(true);

  const nasaData = async () => {
    try {
      const data = await axios.get(
        "https://api.nasa.gov/planetary/apod?start_date=2021-03-07&end_date=2021-03-14&api_key=vn6udhKMZc87KaYKi6DaPPZNjZ4K3niW35MRZbOV"
        );
        console.log(data);
      setNasa(data.data)
    } catch (error) {
      if (error.data) {
        console.log(error.response.data);
    }
    console.log(error);
    }
};


useEffect(() => {
  nasaData();
},[])

const columns = [{
  dataField: 'date',
  text: 'date'
  },  {
    dataField: 'title',
    text: 'title'
}, {
  dataField: 'copyright',
  text: 'copyright'
}];

const rowEvents = {
  onClick: (e, row) => {
    console.log(row)
    setModalInfo(row)
    toggletrueFalse()
  }
}

const toggletrueFalse = () => {
  setShowModal(handleShow)
}

const ModalContent = () => {
  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>
          {modalInfo.title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <img style={{width: "450px", height: "auto"}} src={modalInfo.hdurl}/>
        </div>
        <p>
          {modalInfo.explanation}
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose} >
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
  return (
    <div>
      <h1>NASA</h1>
      <h3>Click on a row to show image and description</h3>
      <br/>
      <br/>
      <BootstrapTable 
          keyField='date' 
          data={ nasa } 
          columns={ columns }
          pagination={paginationFactory()}
          rowEvents={rowEvents}
      />
      {show ? <ModalContent/> : null}
    </div>
    )
}