import { Table } from './_components/table/table'
import "bootstrap/dist/css/bootstrap.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";



function App() {
  return (
    <div className="App">
        <Table/>      
    </div>
  );
}

export default App;
